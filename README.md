# Money.JS

There are four challenges in JavaScript when dealing with money:

- String formatting for output
- String parsing for input
- Floating point precision math
- Currency exchange

Thre are multiple modules that solve these individually, but there isn't
anything in NPM that solves for _all_ cases... until now.

## String formatting

**The problem:**

When you want to display money, there are multiple ways to lay out that
information and various options that you might want for different currencies.

- Currency symbol prefix / suffix
- Delimiters
- Integers vs. decimals

**The solution:**

```js
const money1 = new Money("$1.00")
const money2 = new Money(1)
const money3 = new Money("$1,000.00")
const money4 = new Money("1 000.00 AUD")
const money5 = new Money("1.000,00")
const money6 = new Money("1.000")
const money7 = new Money("1000.000")
const money8 = new Money(1e40)
```

All of these can be treated as a figure of money and

## String parsing

When you want to do math with money, having to parse values out of strings just
to make a simple operation and put it all back into formatting, that's a lot of
back and forth.

## Floating point precision math

At some point to display the amount as a string, you'll need to round or
truncate the floating points, losing accuracy in subsequent operations.

> Consider:

```js
const asNumber = someStringParser("$1.00") // 1
const oneThird = asNumber / 3 // 0.3333333333333333 <- inexact
const asString = someStringFormatter(oneThird) // $0.33 <- VERY inexact

const newNumber = someStringParser(asString) // 0.33
const backToWhole = newNumber * 3 // 0.99
const hopefullyOriginal = someStringFormatter(backToWhole) // $0.99 <- *sad trombone*
```

Some of this can be dealt with in sequencing, but if the information you're
getting from a server is already "capped" because it's a money string, you have
little information as to what that math should have originally been.

> Consider:

The classic floating point precision problem:

```js
const a = someStringParser("$0.10") // 0.1
const b = someStringParser("$0.20") // 0.2
const sum = a + b // <- 0.30000000000000004 Woops
```

This is obviously a small scale problem in this instance, and in many cases it
would be rounded off, but as we saw in the last problem, even simple rounding
can cause a cent difference here and there, making for a confusing on-screen
display in the end.

---

## The `Decimal` Object

The purpose of the Decimal object is to maintain the integrity of the original
values without needing to worry about operating on them over the course of
arithmetic.

One of the main parts of this process is to break down a number (be that a float
or integer) into an integer in scientific notation (also called Standard Form,
more info [here](https://en.wikipedia.org/wiki/Scientific_notation)) so that all
operations are done with sterile whole numbers.

```js
0.1 + 0.2 // Should be 0.3, but isn't. See https://0.30000000000000004.com/
0.1 == 1e-1 // true
0.2 == 2e-1 // true
(1 + 2) * (10 ** -1) // still wrong
String(1 + 2) + "e" + (-1) // '3e-1'
Number(String(1 + 2) + "e" + (-1)) // 0.3 🎉
```

This process _works_, but it's slow to go from number to scientific notation to
string back to number just for a single operation. The `Decimal` object saves
time by keeping all the values in one place and simply updating the internals
using simple math on simple values.

> Note: If you're interested in doing algebra with scientific notation, there
are many great guides, so I'm not going to get into all that here. The above is
how you do addition, and not all things, so if you want to make your own version
of this (or a simplistic version), feel free to fork this repo or start from
fresh using guides you can easily find online.

**Decimals are...**

- **Immutable:** All operations create new Decimals, so that originals are
maintained. Decimals are also frozen objects, so look, don't touch!
- **Extensible:** The `Money` Object builds behaviors on top of `Decimal` and
you can too! If you're using TypeScript, you can even use our types, including
many utilities that go beyond decimal options.
- **Flexible:** If you're comfortable exiting the safety of decimal-land, feel
free! Any decimal can be turned back into standard JS primitives (and other
utility types internal to this module).
- **Type-safe:** TypeScript will warn you early if you're attempting something
that will cause issues, whether that's dealing with `NaN`'s, `Infinity`,
algebraic issues (like dividing by zero).

### Creating a new `Decimal`

You can pass either a number or string representation of a number (in any form).

```js
// All of these create equivalent Decimals
new Decimal(1)
new Decimal("1")
new Decimal(1.0)
new Decimal("1.0")
new Decimal(1e0)
```

### Updating a `Decimal`

> Note: As `Decimal`s maintain immutability, any change you make will result in
a new instance and will not truly "update" your original. Reassign the result or
create a new variable.

#### Duplication

Using the `duplicate()` method, you create an exact copy of the original with a
new object reference.

```js
const original = new Decimal(1)
const duplicate = original.duplicate()

original == duplicate // false
original === duplicate // false
original.asNumber === duplicate.asNumber // true
original.equals(duplicate) // true
```

#### Change exponent of scientific notation on a `Decimal`

There are times when you may want to represent a decimal as a specific exponent
in scientific notation (like 0, which would be the original number from, more on
that in a moment). The `rebaseExponent()` does exactly that.

```js
const original = new Decimal(1.2)
original.asNumber // 1.2
original.asScientificNotation // 12e-1

const rebased = original.rebaseExponent(-2)
rebased.asNumber // 1.2 -- the value is unchanged, only the representation in...
rebased.asScientificNotation // 120e-2
```

One special thing you can do with this is to rebase the exponent of the decimal
to 0 which would make all of the properties equal:

```js
const original = new Decimal(0.5)
const rebased = original.rebaseExponent(0)
rebased.asNumber // 0.5
rebased.asString // '0.5'
rebased.asScientificNotation // 0.5e0 which will be corrected to 0.5
```

For the reasons listed above, it's best to keep the figure before the 'e' as an
integer, but if you want to reverse-engineer the original input without worrying
about the other properties, you can create a decimal that matches on all
properties. Speaking of...

### `Decimal` Properties

These are the properties available for a `Decimal` instance.

#### `asInteger`

From whatever input originally went into creating the decimal, sometimes all you
want is a truncated value as an integer.

```js
new Decimal(1).asInteger // 1
new Decimal("1").asInteger // 1
new Decimal(1.0).asInteger // 1
new Decimal(1e0).asInteger // 1
new Decimal(1e+30).asInteger // 1e+30 -- Too large for JS to represent as an integer
new Decimal(0.9).asInteger // 0
```

#### `asNumber`

When you want the original input, turned into a number, without any special
notation, and _not_ truncated, number is the form that you want.

```js
new Decimal(1).asNumber // 1
new Decimal("1").asNumber // 1
new Decimal(1.0).asNumber // 1
new Decimal(1e0).asNumber // 1
new Decimal(1e+30).asNumber // 1e+30
new Decimal(0.9).asNumber // 0.9
```

#### `asString`

This form will reflect the _value_ of the number, but in the primitive of a
string.

```js
new Decimal(1).asString // "1"
new Decimal("1").asString // "1"
new Decimal(1.0).asString // "1"
new Decimal(1e0).asString // "1"
new Decimal(1e+30).asString // "1e+30"
new Decimal(0.9).asString // "0.9"
```

#### `asScientificNotation`

If you want the scientific notation being used for exponent math, you can get
that too.

```js
new Decimal(1).asScientificNotation // 1e0 *
new Decimal("1").asScientificNotation // 1e0 *
new Decimal(1.0).asScientificNotation // 1e0 *
new Decimal(1e0).asScientificNotation // 1e0 *
new Decimal(1e+30).asScientificNotation // 1e+30
new Decimal(0.9).asScientificNotation // 9e-1
// *- These values will be corrected by JS to 1 if displayed
```

#### `asExponent`

To this point, we've referenced the `Exponent` object before, but you can also
get it right from your `Decimal`. This is the object representation of the
number mentioned above in `asScientificNotation`.

```js
new Decimal(1).asExponent // {base: 1, exponent: 0}
new Decimal("1").asExponent // {base: 1, exponent: 0}
new Decimal(1.0).asExponent // {base: 1, exponent: 0}
new Decimal(1e0).asExponent // {base: 1, exponent: 0}
new Decimal(1e+30).asExponent // {base: 1, exponent: 30}
new Decimal(0.9).asExponent // {base: 9, exponent: -1}
```

> Note: Types for the `Exponent` object are also available from this module.

#### `isInteger`

Sometimes knowing if the module is worth using has value on its own. JavaScript
does a fair job at arithmetic when the operands are integers, so this property
exposes that for you.

```js
new Decimal(1).isInteger // true
new Decimal("1").isInteger // true
new Decimal(1.0).isInteger // true
new Decimal(1e0).isInteger // true
new Decimal(1e+30).isInteger // true
new Decimal(0.9).isInteger // false
new Decimal(1.2).isInteger // false
```

#### `isFloat`

This is the inverse of `isInteger` which recognizes a number that uses floating
point values.

```js
new Decimal(1).isFloat // false
new Decimal("1").isFloat // false
new Decimal(1.0).isFloat // false
new Decimal(1e0).isFloat // false
new Decimal(1e+30).isFloat // false
new Decimal(0.9).isFloat // true
new Decimal(1.2).isFloat // true
```

### Comparing `Decimal`s

Because `Decimal`s are objects, comparison can be tricky, and there are factors
to the way the exponents are expressed that make even futher considerations.
E.g. 1e1 and 10 are equivalent, and comparing 10001e-4 and 1 might be close
enough in some instances.

#### Checking equality

Without any options, the `equals()` method will check strict value equivalence.
This also works for primitives that could be valid inputs to make a new
`Decimal`.

```js
new Decimal(10).equals(new Decimal(1e1)) // true
new Decimal(10).equals(10) // true
new Decimal(10).equals("10") // true
new Decimal(3.14).equals(Math.PI) // false
```

**Within a delta**

If you don't need exact equivalence, you can provide a margin of error for an
inclusive range surrounding the original input.

```js
new Decimal(10).equals(10, {withinDelta: 1}) // true
new Decimal(10).equals(11, {withinDelta: 1}) // true
new Decimal(10).equals(9, {withinDelta: 1}) // true
new Decimal(3.14).equals(Math.PI, {withinDelta: 0.01}) // true
new Decimal(3.14).equals(Math.PI, {withinDelta: 0.001}) // false
```

**Inexact delta**

That range can also be made non-inclusive if you like.

```js
new Decimal(10).equals(9.5, {withinDelta: 1, inclusive: false}) // true
new Decimal(10).equals(11, {withinDelta: 1, inclusive: false}) // false
new Decimal(10).equals(9, {withinDelta: 1, inclusive: false}) // false
new Decimal(3.14).equals(Math.PI, {withinDelta: 0.01, inclusive: false}) // true
new Decimal(3.14).equals(Math.PI, {withinDelta: 0.001, inclusive: false}) // false
```

**Signigicant figures**

If your value can be different past a number of significant figures, that option
is available as well.

> Note: internally, this uses the `toFixed()` method, so only values 0-100 are
available. 1 means 1 digit to the right of the decimal point, which is opposite
of the scientific notation exponent.

```js
new Decimal(10).equals(10, {toPrecision: 1}) // true
new Decimal(10).equals(11, {toPrecision: 1}) // false
new Decimal(10).equals(9, {toPrecision: 1}) // false
new Decimal(3.14).equals(Math.PI, {toPrecision: 2}) // true
new Decimal(3.14).equals(Math.PI, {toPrecision: 3}) // false
```

#### Get comparator of two `Decimal`s

You can get the comparator between two values without knowing how they stack up
as well. There's a Comparator type that you can use, or use values for the
comparisons.

```js
new Decimal(1).compare(0) // Comparator.GT or 1
new Decimal(1).compare(1) // Comparator.EQ or 0
new Decimal(1).compare(2) // Comparator.LT or -1
```

#### Checking if two `Decimal`s are within a certain delta

This is internally used for the `equals()` method when the `withinDelta` option
is given, but you can directly check by using this method. In the same way,
there is an optional third argument for whether or not to make the delta
inclusive.

```js
new Decimal(1).withinDelta(2, 1) // true
new Decimal(1).withinDelta(2, 1, false) // false
new Decimal(1).withinDelta(2, 2) // false
```

### Bulk creation / conversion

Many of the methods on the `Decimal` object take an input and internally convert
it to a `Decimal` instance, but you can do that too if you just want an array of
instances.

```js
// This will make three decimals, with values 1, 2, and 3
// Remember that the new decimal created from the third input will be a brand new instance
Decimal.operandsToDecimals(1, "2", new Decimal(3))
```

### Sign conversion

#### Absolute value

The `absolute()` method (aliased as `abs()` as well) will give you back a new
instance with the absolute value of the original input.

#### Negating a decimal

The `negate()` (or `neg()`) method will invert the sign of the value originally
provided.

### Arithmetic

Because the goal of the `Decimal` object is to internally do all the arithmetic
without errors, all math can be contained in the instance. All of these methods
that take operands use the above `operandsToDecimals()` method to convert all
arguments.

#### Addition

The `add()` method (or `sum()`) will sum all of the arguments with the original
instance that it's being called upon.

```js
new Decimal(0.1).add(0.2).asNumber // 0.3 🎉
new Decimal(1).sum(2, "3", new Decimal(4)).asNumber // 10
```

> Note: None of these methods care about the sign of the operand (unless that
would cause an arithmetic error), so you're welcome to add a negative if you
simply want to sum up a group of mixed sign inputs.

#### Subtraction

The `subtract()` method (or `sub()`, `minus()`, or `min()`) will subtract all
arguments from left-to-right.

```js
new Decimal(10).subtract(1).asNumber // 9
new Decimal(10).sub(3, 2, 1).asNumber // 4
new Decimal(1).minus("2").asNumber // -1
new Decimal(1).min(new Decimal(9e-1)).asNumber // 0.1
```

#### Multiplication

The `multiply()` method (or `mult()` or `product()`) multiplies all arguments
together.

```js
new Decimal(1).multiply(10 ** 2).asScientificNotation // 1e2
new Decimal(0.1).mult(0.2).asNumber // 0.02 🎉 <- Another common JS issue
new Decimal(3).product(4, 3).asNumber // 36
```

#### Division

The `divide()` method (or `div()`) will divide all the arguments (once again,
left-to-right). This also returns an error when attempting to divide by zero.

```js
new Decimal(3).divide(2).asNumber // 1.5
new Decimal(6).div(3, 2).asNumber // 1
new Decimal(1).div(0) // Error
```

#### Modulo / remainder

The `modulo()` method (or `mod()`, `remainder()`, or `rem()`) will return the
remainder. This can still be used left-to-right with multiple arguments.

```js
new Decimal(5).modulo(2).asNumber // 1
new Decimal(12).mod(7, 3).asNumber // 2
new Decimal(16).remainder(4).asNumber // 0
```

#### Exponentiation

The `exponentiate()` method (or `exp()`, `power()`, `pow()`, or `raise()`)
raises the instance to the argument (and continues applying the result to the
next argument) if multiple are given.

```js
new Decimal(3).exponentiate(3).asNumber // 27
new Decimal(2).exp(2, 2, 2).asNumber // 256 <- Which JS tends to get wrong
2 ** 2 ** 2 ** 2 // 65536
(((2 ** 2) ** 2) ** 2) // 256 🤔 function application 🤷‍♂️
```

#### Square root

The `squareRoot()` method (or `sqrt()`) _doesn't_ take an argument, but will
give the square root as a new `Decimal`.

```js
new Decimal(16).squareRoot().asNumber // 4
```

---

## The `Money` Object
