import { errorByName } from "./error";
import { isError } from "./result";
var safeOperation = function (result) {
    if (isError(result))
        return result;
    if (Number.isNaN(result))
        return errorByName("NaN");
    if (!Number.isFinite(result))
        return errorByName("Infinite");
    return result;
};
export var add = function (l, r) { return safeOperation(l + r); };
export var sub = function (l, r) { return safeOperation(l - r); };
export var mult = function (l, r) { return safeOperation(l * r); };
export var div = function (l, r) {
    return r === 0 ? errorByName("divideByZero") : safeOperation(l / r);
};
export var rem = function (l, r) {
    return r === 0 ? errorByName("divideByZero") : safeOperation(l % r);
};
export var pow = function (l, r) { return safeOperation(Math.pow(l, r)); };
export var sqrt = function (n) {
    if (Number.isNaN(n))
        errorByName("NaN");
    if (n < 0)
        return errorByName("negSquareRoot");
    return safeOperation(Math.sqrt(n));
};
