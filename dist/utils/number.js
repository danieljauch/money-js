import { delimiterRejoin, normalizeNumber, rejoinParts } from "./normalize";
export function numberParts(rawInput, decimal) {
    if (decimal === void 0) { decimal = "."; }
    var normalizedNumber = normalizeNumber(rawInput, decimal);
    var _a = String(normalizedNumber).split("."), left = _a[0], right = _a[1];
    return { left: left, right: right };
}
export function numberPartsToString(parts, options) {
    if (options)
        return delimiterRejoin(parts, options.delimiter, options.decimal);
    var left = parts.left, right = parts.right;
    return rejoinParts([left, right], ".");
}
export function numberPartsToNumber(parts) {
    return Number(numberPartsToString(parts));
}
