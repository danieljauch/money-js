import { normalizeNumber, rejoinParts } from "./normalize";
import { numberParts } from "./number";
var defaultOptions = {
    delimiter: ",",
    decimal: "."
};
var Exponent = (function () {
    function Exponent(rawInput, options) {
        var _this = this;
        if (options === void 0) { options = defaultOptions; }
        this.getParts = function (rawInput, options) {
            if (options === void 0) { options = defaultOptions; }
            var _a = options.decimal, decimal = _a === void 0 ? defaultOptions.decimal : _a, withExponent = options.withExponent;
            var normalizedNumber = normalizeNumber(rawInput, decimal);
            var _b = normalizeNumber(rawInput)
                .toExponential()
                .split("e"), stringBase = _b[0], stringExponent = _b[1];
            var base = parseFloat(stringBase);
            var exponent = parseInt(stringExponent);
            var output;
            if (Number.isInteger(base))
                output = { base: base, exponent: exponent };
            else {
                var _c = numberParts(normalizedNumber), left = _c.left, right = _c.right;
                if (!right)
                    output = { base: parseInt(left), exponent: 0 };
                else
                    output = { base: Number(left + right), exponent: -right.length };
            }
            if (withExponent !== undefined && withExponent !== output.exponent)
                return {
                    exponent: withExponent,
                    base: withExponent === 0
                        ? Number(normalizedNumber)
                        : base * Math.pow(10, exponent + Math.abs(withExponent))
                };
            return output;
        };
        this.toString = function () {
            if (_this.base === 0)
                return "0";
            if (_this.exponent === 0)
                return String(_this.base);
            return rejoinParts([_this.base, _this.exponent], "e");
        };
        var _a = this.getParts(rawInput, options), base = _a.base, exponent = _a.exponent;
        this.base = base;
        this.exponent = exponent;
        this.asString = this.toString();
        this.asNumber = Number(this.asString);
    }
    return Exponent;
}());
export default Exponent;
