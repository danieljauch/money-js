export var delimiterRejoin = function (_a, delimiter, decimal) {
    var left = _a.left, right = _a.right;
    if (delimiter === void 0) { delimiter = ","; }
    if (decimal === void 0) { decimal = "."; }
    var stringLeft = String(left);
    if (stringLeft.length <= 3) {
        if (!right)
            return stringLeft;
        return stringLeft + decimal + String(right);
    }
    var leftDigits = stringLeft.split("");
    var leftPadding = leftDigits.length % 3;
    var leftThread = [];
    for (var i = leftDigits.length - 1; i >= 0; i--) {
        leftThread.push(leftDigits[i]);
        if (i !== 0 && i > leftPadding - 1 && (i - leftPadding) % 3 === 0)
            leftThread.push(delimiter);
    }
    var delimitedLeft = leftThread.reverse().join("");
    if (!right)
        return delimitedLeft;
    return delimitedLeft + decimal + String(right);
};
export var normalizeNumber = function (rawInput, decimal) {
    if (decimal === void 0) { decimal = "."; }
    if (typeof rawInput === "number")
        return rawInput;
    var output = rawInput;
    if (decimal !== ".")
        output = rawInput.replace(".", "").replace(decimal, ".");
    return Number(output.replace(/[^\d.e-]+/, ""));
};
export var rejoinParts = function (parts, joiner) { return parts.filter(function (value) { return !!value; }).join(joiner); };
