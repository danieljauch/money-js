import Lexicon from "./lexicon";
export var errorByName = function (name) {
    return new Error(Lexicon.error[name]);
};
