export var isError = function (result) {
    return result instanceof Error;
};
export var isSuccess = function (result) { return !isError(result); };
