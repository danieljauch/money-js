export var inRange = function (member, _a) {
    var bottom = _a[0], top = _a[1];
    return member >= bottom && member <= top;
};
