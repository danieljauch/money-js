import Decimal from "./decimal";
import { isSuccess } from "./utils/result";
export function mathTest(jsExpression, decimalExpression, matcher) {
    expect(jsExpression).not.toBe(matcher);
    expect(isSuccess(decimalExpression)).toBe(true);
    if (isSuccess(decimalExpression))
        expect(decimalExpression.asNumber).toBe(matcher);
}
export var addTest = function (input1, input2, matcher) {
    return mathTest(input1 + input2, new Decimal(input1).add(input2), matcher);
};
export var subTest = function (input1, input2, matcher) {
    return mathTest(input1 - input2, new Decimal(input1).sub(input2), matcher);
};
export var multTest = function (input1, input2, matcher) {
    return mathTest(input1 * input2, new Decimal(input1).mult(input2), matcher);
};
export var divTest = function (input1, input2, matcher) {
    return mathTest(input1 / input2, new Decimal(input1).div(input2), matcher);
};
