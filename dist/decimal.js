var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import { Comparator } from "./types";
import * as Algebra from "./utils/algebra";
import { errorByName } from "./utils/error";
import Exponent from "./utils/exponent";
import { numberParts, numberPartsToNumber, numberPartsToString } from "./utils/number";
import { inRange } from "./utils/range";
import { isError } from "./utils/result";
var defaultOptions = {
    delimiter: ",",
    decimal: "."
};
var PRECISION_RANGE = [0, 100];
var MIN_TO_PRECISION = PRECISION_RANGE[0], MAX_TO_PRECISION = PRECISION_RANGE[1];
var RADIX_RANGE = [2, 36];
var Decimal = (function () {
    function Decimal(rawInput, options) {
        var _this = this;
        if (options === void 0) { options = defaultOptions; }
        this.asStringOutput = function (numberObj, options) {
            var baseString = options.decimal !== undefined && options.delimiter !== undefined
                ? numberPartsToString(numberObj, {
                    decimal: options.decimal,
                    delimiter: options.delimiter
                })
                : numberPartsToString(numberObj);
            if (options.unit) {
                var unit = options.unit, unitPosition = options.unitPosition;
                if (unitPosition === "prefix")
                    return unit + baseString;
                return baseString + unit;
            }
            return baseString;
        };
        this.duplicate = function (options) {
            if (options === void 0) { options = defaultOptions; }
            return new Decimal(_this.__original__, options);
        };
        this.rebaseExponent = function (to) {
            return _this.duplicate(__assign(__assign({}, defaultOptions), { withExponent: to }));
        };
        this.toRadix = function (radix) {
            if (!inRange(radix, RADIX_RANGE))
                return errorByName("outsideRange");
            return parseFloat(String(_this.asNumber)).toString(radix);
        };
        this.equals = function (other, _a) {
            var _b = _a === void 0 ? {
                inclusive: true,
                toPrecision: MAX_TO_PRECISION,
                withinDelta: 0
            } : _a, inclusive = _b.inclusive, toPrecision = _b.toPrecision, withinDelta = _b.withinDelta;
            var otherDecimal = Decimal.operandsToDecimals(other)[0];
            var thisExponent = _this.asExponent;
            var otherExponent = otherDecimal.asExponent;
            var thisBase = thisExponent.base;
            var otherBase = otherExponent.base;
            if (thisExponent.exponent !== otherExponent.exponent) {
                thisBase = _this.asNumber;
                otherBase = otherDecimal.asNumber;
            }
            if (toPrecision !== undefined &&
                toPrecision < MAX_TO_PRECISION &&
                toPrecision >= MIN_TO_PRECISION) {
                thisBase = parseFloat(thisBase.toFixed(toPrecision));
                otherBase = parseFloat(otherBase.toFixed(toPrecision));
            }
            if (toPrecision !== undefined && toPrecision < MIN_TO_PRECISION)
                return new RangeError("toFixed() digits argument must be between 0 and 100");
            if ((typeof withinDelta === "number" && withinDelta > 0) ||
                (withinDelta instanceof Decimal && withinDelta.asNumber > 0))
                return _this.withinDelta(otherDecimal, withinDelta, inclusive !== undefined ? inclusive : true);
            return thisBase === otherBase;
        };
        this.compare = function (other) {
            var otherDecimal = Decimal.operandsToDecimals(other)[0];
            if (_this.equals(otherDecimal))
                return Comparator.EQ;
            var thisExponent = _this.asExponent;
            var otherExponent = otherDecimal.asExponent;
            if (thisExponent.exponent === otherExponent.exponent) {
                if (thisExponent.base > otherExponent.base)
                    return Comparator.GT;
                return Comparator.LT;
            }
            if (_this.asNumber > otherDecimal.asNumber)
                return Comparator.GT;
            return Comparator.LT;
        };
        this.withinDelta = function (other, delta, inclusive) {
            if (inclusive === void 0) { inclusive = true; }
            var otherDecimal = Decimal.operandsToDecimals(other)[0];
            var _a = [
                _this.sub(delta),
                _this.add(delta)
            ], lowerLimit = _a[0], upperLimit = _a[1];
            if (isError(lowerLimit) || isError(upperLimit))
                return false;
            if (inclusive)
                return ([Comparator.GT, Comparator.EQ].includes(otherDecimal.compare(lowerLimit)) &&
                    [Comparator.LT, Comparator.EQ].includes(otherDecimal.compare(upperLimit)));
            return (otherDecimal.compare(lowerLimit) === Comparator.GT &&
                otherDecimal.compare(upperLimit) === Comparator.LT);
        };
        this.absolute = function () {
            return new Decimal(_this.asInteger < 0 ? -_this.asNumber : _this.asNumber);
        };
        this.abs = this.absolute;
        this.negate = function () { return new Decimal(-_this.asNumber); };
        this.neg = this.negate;
        this.mathReducer = function (reducer) {
            var operands = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                operands[_i - 1] = arguments[_i];
            }
            return Decimal.operandsToDecimals.apply(Decimal, operands).reduce(function (left, right) {
                return isError(left) ? left : reducer(left, right);
            }, _this);
        };
        this.baseOpReducer = function (baseOperator) {
            var operands = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                operands[_i - 1] = arguments[_i];
            }
            return _this.mathReducer.apply(_this, __spreadArrays([function (left, right) {
                    var _a = left.asExponent, leftBase = _a.base, leftExponent = _a.exponent;
                    var _b = right.asExponent, rightBase = _b.base, rightExponent = _b.exponent;
                    var base, exponent;
                    if (leftExponent === rightExponent) {
                        base = baseOperator(leftBase, rightBase);
                        exponent = leftExponent;
                    }
                    else {
                        var lowestExponent_1 = Math.min(leftExponent, rightExponent);
                        var lowestBase = function (input) {
                            return input.asExponent.exponent === lowestExponent_1
                                ? input.asExponent.base
                                : input.rebaseExponent(lowestExponent_1).asExponent.base;
                        };
                        base = baseOperator(lowestBase(left), lowestBase(right));
                        exponent = lowestExponent_1;
                    }
                    if (isError(base))
                        return base;
                    return new Decimal(Number(exponent ? base + "e" + exponent : base));
                }], operands));
        };
        this.add = function () {
            var operands = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operands[_i] = arguments[_i];
            }
            return _this.baseOpReducer.apply(_this, __spreadArrays([Algebra.add], operands));
        };
        this.sum = this.add;
        this.subtract = function () {
            var operands = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operands[_i] = arguments[_i];
            }
            return _this.baseOpReducer.apply(_this, __spreadArrays([Algebra.sub], operands));
        };
        this.sub = this.subtract;
        this.minus = this.subtract;
        this.min = this.subtract;
        this.exponentOpReducer = function (baseOperator, exponentOperator) {
            var operands = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                operands[_i - 2] = arguments[_i];
            }
            return _this.mathReducer.apply(_this, __spreadArrays([function (left, right) {
                    var _a = left.asExponent, leftBase = _a.base, leftExponent = _a.exponent;
                    var _b = right.asExponent, rightBase = _b.base, rightExponent = _b.exponent;
                    var base = baseOperator(leftBase, rightBase);
                    if (isError(base))
                        return base;
                    var exponent = exponentOperator(leftExponent, rightExponent);
                    if (isError(exponent))
                        return exponent;
                    return new Decimal(Number(exponent ? base + "e" + exponent : base));
                }], operands));
        };
        this.multiply = function () {
            var operands = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operands[_i] = arguments[_i];
            }
            return _this.exponentOpReducer.apply(_this, __spreadArrays([Algebra.mult, Algebra.add], operands));
        };
        this.mult = this.multiply;
        this.product = this.multiply;
        this.divide = function () {
            var operands = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operands[_i] = arguments[_i];
            }
            return operands.includes(0) || operands.includes("0")
                ? errorByName("divideByZero")
                : _this.exponentOpReducer.apply(_this, __spreadArrays([Algebra.div, Algebra.sub], operands));
        };
        this.div = this.divide;
        this.modulo = function () {
            var operands = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operands[_i] = arguments[_i];
            }
            return operands.includes(0) || operands.includes("0")
                ? errorByName("divideByZero")
                : _this.baseOpReducer.apply(_this, __spreadArrays([Algebra.rem], operands));
        };
        this.mod = this.modulo;
        this.remainder = this.modulo;
        this.rem = this.modulo;
        this.exponentiate = function () {
            var operands = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operands[_i] = arguments[_i];
            }
            return _this.baseOpReducer.apply(_this, __spreadArrays([Algebra.pow], operands));
        };
        this.exp = this.exponentiate;
        this.power = this.exponentiate;
        this.pow = this.exponentiate;
        this.raise = this.exponentiate;
        this.squareRoot = function () {
            var root = Algebra.sqrt(_this.asNumber);
            if (isError(root))
                return root;
            return new Decimal(root);
        };
        this.sqrt = this.squareRoot;
        var _a = options.decimal, decimal = _a === void 0 ? "." : _a, _b = options.delimiter, delimiter = _b === void 0 ? "," : _b;
        var exponent = new Exponent(rawInput, options);
        var numberObj = numberParts(rawInput, decimal);
        this.__original__ = rawInput;
        this.asInteger = Number(numberObj.left);
        this.asNumber = numberPartsToNumber(numberObj);
        this.asString = this.asStringOutput(numberObj, options);
        this.asScientificNotation = exponent.asNumber;
        this.asBinary = this.toRadix(2);
        this.asOctal = this.toRadix(8);
        this.asHexidecimal = this.toRadix(16);
        this.isInteger = this.asInteger === this.asNumber;
        this.isFloat = !this.isInteger;
        this.asExponent = exponent;
        this.delimiter = delimiter;
        this.decimal = decimal;
        Object.freeze(this);
    }
    Decimal.fromExponentParts = function (_a) {
        var base = _a.base, exponent = _a.exponent;
        if (base === 0)
            return new Decimal(0);
        if (exponent === 0)
            return new Decimal(base);
        return new Decimal(base + "e" + exponent);
    };
    Decimal.operandsToDecimals = function () {
        var operands = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            operands[_i] = arguments[_i];
        }
        return operands.map(function (operand) {
            return operand instanceof Decimal ? operand : new Decimal(operand);
        });
    };
    return Decimal;
}());
export default Decimal;
