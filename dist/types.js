export var Comparator;
(function (Comparator) {
    Comparator[Comparator["LT"] = -1] = "LT";
    Comparator[Comparator["EQ"] = 0] = "EQ";
    Comparator[Comparator["GT"] = 1] = "GT";
})(Comparator || (Comparator = {}));
