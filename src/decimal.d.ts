import {Result} from "./utils/result.d"
import {Delimiter, InputShape} from "./types"

import Decimal from "./decimal"

export interface DecimalOptions {
  delimiter?: Delimiter
  decimal?: Delimiter
  withExponent?: number
  unit?: string
  unitPosition?: "prefix" | "suffix"
}

export type DecimalOperand = Decimal | InputShape

export type DecimalOperation = (
  ...operands: DecimalOperand[]
) => Result<Decimal>
