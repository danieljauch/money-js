import {Result} from "./utils/result.d"

import Decimal from "./decimal"
import {isSuccess} from "./utils/result"

export function mathTest (
  jsExpression: number,
  decimalExpression: Result<Decimal>,
  matcher: number
): void {
  expect(jsExpression).not.toBe(matcher)
  expect(isSuccess(decimalExpression)).toBe(true)
  if (isSuccess(decimalExpression))
    expect(decimalExpression.asNumber).toBe(matcher)
}

type TestHelper = (input1: number, input2: number, matcher: number) => void

export const addTest: TestHelper = (input1, input2, matcher) =>
  mathTest(input1 + input2, new Decimal(input1).add(input2), matcher)
export const subTest: TestHelper = (input1, input2, matcher) =>
  mathTest(input1 - input2, new Decimal(input1).sub(input2), matcher)
export const multTest: TestHelper = (input1, input2, matcher) =>
  mathTest(input1 * input2, new Decimal(input1).mult(input2), matcher)
export const divTest: TestHelper = (input1, input2, matcher) =>
  mathTest(input1 / input2, new Decimal(input1).div(input2), matcher)
