import {Equation} from "./utils/algebra.d"
import {DecimalOperand, DecimalOperation, DecimalOptions} from "./decimal.d"
import {ExponentParts} from "./utils/exponent.d"
import {NumberParts} from "./utils/number.d"
import {Range} from "./utils/range.d"
import {Result} from "./utils/result.d"
import {Comparator, Delimiter, EqualsOptions, InputShape} from "./types"

import * as Algebra from "./utils/algebra"
import {errorByName} from "./utils/error"
import Exponent from "./utils/exponent"
import {
  numberParts,
  numberPartsToNumber,
  numberPartsToString
} from "./utils/number"
import {inRange} from "./utils/range"
import {isError} from "./utils/result"

const defaultOptions: DecimalOptions = {
  delimiter: ",",
  decimal: "."
}
const PRECISION_RANGE: Range<number> = [0, 100]
const [MIN_TO_PRECISION, MAX_TO_PRECISION] = PRECISION_RANGE
const RADIX_RANGE: Range<number> = [2, 36]

export default class Decimal {
  __original__: number | string
  asInteger: number
  asNumber: number
  asString: string
  asScientificNotation: number
  asBinary: Result<string>
  asOctal: Result<string>
  asHexidecimal: Result<string>
  isInteger: boolean
  isFloat: boolean
  asExponent: Exponent
  delimiter: Delimiter
  decimal: Delimiter

  constructor (rawInput: InputShape, options: DecimalOptions = defaultOptions) {
    const {decimal = ".", delimiter = ","} = options
    const exponent = new Exponent(rawInput, options)
    const numberObj = numberParts(rawInput, decimal)

    this.__original__ = rawInput
    this.asInteger = Number(numberObj.left)
    this.asNumber = numberPartsToNumber(numberObj)
    this.asString = this.asStringOutput(numberObj, options)
    this.asScientificNotation = exponent.asNumber
    this.asBinary = this.toRadix(2)
    this.asOctal = this.toRadix(8)
    this.asHexidecimal = this.toRadix(16)
    this.isInteger = this.asInteger === this.asNumber
    this.isFloat = !this.isInteger
    this.asExponent = exponent
    this.delimiter = delimiter
    this.decimal = decimal
    Object.freeze(this)
  }

  private asStringOutput = (
    numberObj: NumberParts,
    options: DecimalOptions
  ): string => {
    const baseString =
      options.decimal !== undefined && options.delimiter !== undefined
        ? numberPartsToString(numberObj, {
            decimal: options.decimal,
            delimiter: options.delimiter
          })
        : numberPartsToString(numberObj)
    if (options.unit) {
      const {unit, unitPosition} = options
      if (unitPosition === "prefix") return unit + baseString
      return baseString + unit
    }

    return baseString
  }

  // ---------------------------- Recreation / edits ---------------------------
  duplicate = (options: DecimalOptions = defaultOptions): Decimal =>
    new Decimal(this.__original__, options)

  rebaseExponent = (to: number): Decimal =>
    this.duplicate({...defaultOptions, withExponent: to})

  static fromExponentParts = ({base, exponent}: ExponentParts): Decimal => {
    if (base === 0) return new Decimal(0)
    if (exponent === 0) return new Decimal(base)
    return new Decimal(`${base}e${exponent}`)
  }

  toRadix = (radix: number): Result<string> => {
    if (!inRange<number>(radix, RADIX_RANGE)) return errorByName("outsideRange")
    return parseFloat(String(this.asNumber)).toString(radix)
  }

  // -------------------------------- Comparison -------------------------------
  equals = (
    other: DecimalOperand,
    {inclusive, toPrecision, withinDelta}: EqualsOptions = {
      inclusive: true,
      toPrecision: MAX_TO_PRECISION,
      withinDelta: 0
    }
  ): Result<boolean> => {
    const [otherDecimal] = Decimal.operandsToDecimals(other)
    const thisExponent = this.asExponent
    const otherExponent = otherDecimal.asExponent
    let thisBase = thisExponent.base
    let otherBase = otherExponent.base

    if (thisExponent.exponent !== otherExponent.exponent) {
      thisBase = this.asNumber
      otherBase = otherDecimal.asNumber
    }

    if (
      toPrecision !== undefined &&
      toPrecision < MAX_TO_PRECISION &&
      toPrecision >= MIN_TO_PRECISION
    ) {
      thisBase = parseFloat(thisBase.toFixed(toPrecision))
      otherBase = parseFloat(otherBase.toFixed(toPrecision))
    }
    if (toPrecision !== undefined && toPrecision < MIN_TO_PRECISION)
      return new RangeError(
        "toFixed() digits argument must be between 0 and 100"
      )

    if (
      (typeof withinDelta === "number" && withinDelta > 0) ||
      (withinDelta instanceof Decimal && withinDelta.asNumber > 0)
    )
      return this.withinDelta(
        otherDecimal,
        withinDelta,
        inclusive !== undefined ? inclusive : true
      )

    return thisBase === otherBase
  }

  compare = (other: DecimalOperand): Comparator => {
    const [otherDecimal] = Decimal.operandsToDecimals(other)
    if (this.equals(otherDecimal)) return Comparator.EQ

    const thisExponent = this.asExponent
    const otherExponent = otherDecimal.asExponent

    if (thisExponent.exponent === otherExponent.exponent) {
      if (thisExponent.base > otherExponent.base) return Comparator.GT
      return Comparator.LT
    }

    if (this.asNumber > otherDecimal.asNumber) return Comparator.GT
    return Comparator.LT
  }

  withinDelta = (
    other: DecimalOperand,
    delta: DecimalOperand,
    inclusive = true
  ): boolean => {
    const [otherDecimal] = Decimal.operandsToDecimals(other)
    const [lowerLimit, upperLimit]: Range<Result<Decimal>> = [
      this.sub(delta),
      this.add(delta)
    ]
    if (isError(lowerLimit) || isError(upperLimit)) return false

    if (inclusive)
      return (
        [Comparator.GT, Comparator.EQ].includes(
          otherDecimal.compare(lowerLimit)
        ) &&
        [Comparator.LT, Comparator.EQ].includes(
          otherDecimal.compare(upperLimit)
        )
      )

    return (
      otherDecimal.compare(lowerLimit) === Comparator.GT &&
      otherDecimal.compare(upperLimit) === Comparator.LT
    )
  }

  // ----------------------------- Bulk conversion -----------------------------
  static operandsToDecimals = (...operands: DecimalOperand[]): Decimal[] =>
    operands.map(
      (operand: DecimalOperand) =>
        operand instanceof Decimal ? operand : new Decimal(operand)
    )

  // Sign conversion
  absolute = (): Decimal =>
    new Decimal(this.asInteger < 0 ? -this.asNumber : this.asNumber)
  abs = this.absolute

  negate = (): Decimal => new Decimal(-this.asNumber)
  neg = this.negate

  // -------------------------------- Arithmetic -------------------------------
  private mathReducer = (
    reducer: (left: Decimal, right: Decimal) => Result<Decimal>,
    ...operands: DecimalOperand[]
  ): Result<Decimal> =>
    Decimal.operandsToDecimals(...operands).reduce(
      (left: Result<Decimal>, right: Decimal): Result<Decimal> =>
        isError(left) ? left : reducer(left, right),
      this
    )

  private baseOpReducer = (
    baseOperator: Equation,
    ...operands: DecimalOperand[]
  ): Result<Decimal> =>
    this.mathReducer((left, right) => {
      const {base: leftBase, exponent: leftExponent} = left.asExponent
      const {base: rightBase, exponent: rightExponent} = right.asExponent
      let base, exponent

      if (leftExponent === rightExponent) {
        base = baseOperator(leftBase, rightBase)
        exponent = leftExponent
      } else {
        const lowestExponent = Math.min(leftExponent, rightExponent)
        const lowestBase = (input: Decimal): number =>
          input.asExponent.exponent === lowestExponent
            ? input.asExponent.base
            : input.rebaseExponent(lowestExponent).asExponent.base

        base = baseOperator(lowestBase(left), lowestBase(right))
        exponent = lowestExponent
      }

      if (isError(base)) return base
      return new Decimal(Number(exponent ? `${base}e${exponent}` : base))
    }, ...operands)

  add: DecimalOperation = (...operands) =>
    this.baseOpReducer(Algebra.add, ...operands)
  sum = this.add

  subtract: DecimalOperation = (...operands) =>
    this.baseOpReducer(Algebra.sub, ...operands)
  sub = this.subtract
  minus = this.subtract
  min = this.subtract

  private exponentOpReducer = (
    baseOperator: Equation,
    exponentOperator: Equation,
    ...operands: DecimalOperand[]
  ): Result<Decimal> =>
    this.mathReducer((left, right) => {
      const {base: leftBase, exponent: leftExponent} = left.asExponent
      const {base: rightBase, exponent: rightExponent} = right.asExponent

      const base = baseOperator(leftBase, rightBase)
      if (isError(base)) return base

      const exponent = exponentOperator(leftExponent, rightExponent)
      if (isError(exponent)) return exponent

      return new Decimal(Number(exponent ? `${base}e${exponent}` : base))
    }, ...operands)

  multiply: DecimalOperation = (...operands) =>
    this.exponentOpReducer(Algebra.mult, Algebra.add, ...operands)
  mult = this.multiply
  product = this.multiply

  divide: DecimalOperation = (...operands) =>
    operands.includes(0) || operands.includes("0")
      ? errorByName("divideByZero")
      : this.exponentOpReducer(Algebra.div, Algebra.sub, ...operands)
  div = this.divide

  modulo: DecimalOperation = (...operands) =>
    operands.includes(0) || operands.includes("0")
      ? errorByName("divideByZero")
      : this.baseOpReducer(Algebra.rem, ...operands)
  mod = this.modulo
  remainder = this.modulo
  rem = this.modulo

  exponentiate: DecimalOperation = (...operands) =>
    this.baseOpReducer(Algebra.pow, ...operands)
  exp = this.exponentiate
  power = this.exponentiate
  pow = this.exponentiate
  raise = this.exponentiate

  squareRoot = (): Result<Decimal> => {
    const root = Algebra.sqrt(this.asNumber)
    if (isError(root)) return root
    return new Decimal(root)
  }
  sqrt = this.squareRoot
}
