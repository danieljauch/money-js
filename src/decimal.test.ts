import {Comparator} from "./types"

import Decimal from "./decimal"
import {addTest, subTest, multTest} from "./test-helper"

// -------------------------------- Constructor --------------------------------
describe("new Decimal()", () => {
  it("can take a string as input", () => {
    expect(new Decimal("1")).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
    expect(new Decimal("20")).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 1
        }),
        asInteger: 20,
        asNumber: 20,
        asScientificNotation: 2e1,
        asString: "20"
      })
    )
    expect(new Decimal("4.005")).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 4005,
          exponent: -3
        }),
        asInteger: 4,
        asNumber: 4.005,
        asScientificNotation: 4005e-3,
        asString: "4.005"
      })
    )
  })

  it("can take a integer as input", () => {
    expect(new Decimal(1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
    expect(new Decimal(20)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 1
        }),
        asInteger: 20,
        asNumber: 20,
        asScientificNotation: 2e1,
        asString: "20"
      })
    )
  })

  it("can take a float as input", () => {
    expect(new Decimal(0.1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: -1
        }),
        asInteger: 0,
        asNumber: 0.1,
        asScientificNotation: 1e-1,
        asString: "0.1"
      })
    )
    expect(new Decimal(2.3)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 23,
          exponent: -1
        }),
        asInteger: 2,
        asNumber: 2.3,
        asScientificNotation: 23e-1,
        asString: "2.3"
      })
    )
    expect(new Decimal(4.005)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 4005,
          exponent: -3
        }),
        asInteger: 4,
        asNumber: 4.005,
        asScientificNotation: 4005e-3,
        asString: "4.005"
      })
    )
    expect(new Decimal(600)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 6,
          exponent: 2
        }),
        asInteger: 600,
        asNumber: 600,
        asScientificNotation: 6e2,
        asString: "600"
      })
    )
  })

  it("can take a scientific notation as input", () => {
    expect(new Decimal(1e-1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: -1
        }),
        asInteger: 0,
        asNumber: 0.1,
        asScientificNotation: 1e-1,
        asString: "0.1"
      })
    )
    expect(new Decimal(23e-1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 23,
          exponent: -1
        }),
        asInteger: 2,
        asNumber: 2.3,
        asScientificNotation: 23e-1,
        asString: "2.3"
      })
    )
    expect(new Decimal(4005e-3)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 4005,
          exponent: -3
        }),
        asInteger: 4,
        asNumber: 4.005,
        asScientificNotation: 4005e-3,
        asString: "4.005"
      })
    )
    expect(new Decimal(6e2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 6,
          exponent: 2
        }),
        asInteger: 600,
        asNumber: 600,
        asScientificNotation: 6e2,
        asString: "600"
      })
    )
    expect(new Decimal("7e2")).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 7,
          exponent: 2
        }),
        asInteger: 700,
        asNumber: 700,
        asScientificNotation: 7e2,
        asString: "700"
      })
    )
  })

  it("works with decimal and delimiter options", () => {
    expect(new Decimal(1234.56, {decimal: ",", delimiter: "."})).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 123456,
          exponent: -2
        }),
        asInteger: 1234,
        asNumber: 1234.56,
        asScientificNotation: 123456e-2,
        asString: "1.234,56"
      })
    )
    expect(new Decimal(1234.56)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 123456,
          exponent: -2
        }),
        asInteger: 1234,
        asNumber: 1234.56,
        asScientificNotation: 123456e-2,
        asString: "1,234.56"
      })
    )
  })

  it("works with unit and position options", () => {
    expect(new Decimal(1.23, {unit: "$", unitPosition: "prefix"})).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 123,
          exponent: -2
        }),
        asInteger: 1,
        asNumber: 1.23,
        asScientificNotation: 123e-2,
        asString: "$1.23"
      })
    )
    expect(new Decimal(1.23, {unit: " USD", unitPosition: "suffix"})).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 123,
          exponent: -2
        }),
        asInteger: 1,
        asNumber: 1.23,
        asScientificNotation: 123e-2,
        asString: "1.23 USD"
      })
    )
    expect(new Decimal(1.23, {unit: " USD"})).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 123,
          exponent: -2
        }),
        asInteger: 1,
        asNumber: 1.23,
        asScientificNotation: 123e-2,
        asString: "1.23 USD"
      })
    )
  })

  it("works with all options", () => {
    expect(
      new Decimal(70100200300.45, {
        unit: "£",
        unitPosition: "prefix",
        delimiter: ".",
        decimal: ","
      })
    ).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 7010020030045,
          exponent: -2
        }),
        asInteger: 70100200300,
        asNumber: 70100200300.45,
        asScientificNotation: 7010020030045e-2,
        asString: "£70.100.200.300,45"
      })
    )
  })
})

// ----------------------------- Recreation / edits ----------------------------
describe("duplicate()", () => {
  it("makes a copy of the decimal", () => {
    const decimal = new Decimal(1)
    const duplicate = decimal.duplicate()

    expect(decimal == duplicate).toBe(false)
    expect(decimal === duplicate).toBe(false)

    expect(decimal.asInteger).toBe(duplicate.asInteger)
    expect(decimal.asNumber).toBe(duplicate.asNumber)
    expect(decimal.asString).toBe(duplicate.asString)
    expect(decimal.asScientificNotation).toBe(duplicate.asScientificNotation)
    expect(decimal.asExponent.base).toBe(duplicate.asExponent.base)
    expect(decimal.asExponent.exponent).toBe(duplicate.asExponent.exponent)
    expect(decimal.asExponent.asNumber).toBe(duplicate.asExponent.asNumber)
    expect(decimal.asExponent.asString).toBe(duplicate.asExponent.asString)
  })
})

describe("rebaseExponent()", () => {
  it("returns a duplicate when the decimal is already at that exponent", () => {
    const original = new Decimal(1)
    const shifted = original.rebaseExponent(0)

    expect(original == shifted).toBe(false)
    expect(original === shifted).toBe(false)
    expect(shifted).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("makes an adjusted decimal when given a new exponent", () => {
    expect(new Decimal(1).rebaseExponent(-1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 10,
          exponent: -1
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 10e-1,
        asString: "1"
      })
    )
    expect(new Decimal(1).rebaseExponent(1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 10,
          exponent: 1
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 10e1,
        asString: "1"
      })
    )
    expect(new Decimal(10).rebaseExponent(0)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 10,
          exponent: 0
        }),
        asInteger: 10,
        asNumber: 10,
        asScientificNotation: 10,
        asString: "10"
      })
    )
  })

  it("rebases when there are numbers on both sides of the decimal point", () => {
    expect(new Decimal(1.23).rebaseExponent(0)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1.23,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1.23,
        asScientificNotation: 1.23,
        asString: "1.23"
      })
    )
  })
})

describe("fromExponentParts()", () => {
  it("ignores the exponent if the base is 0", () => {
    expect(Decimal.fromExponentParts({base: 0, exponent: 1000})).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 0,
          exponent: 0
        }),
        asInteger: 0,
        asNumber: 0,
        asScientificNotation: 0,
        asString: "0"
      })
    )
  })

  it("uses the base if the exponent is 0", () => {
    expect(Decimal.fromExponentParts({base: 123, exponent: 0})).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 123,
          exponent: 0
        }),
        asInteger: 123,
        asNumber: 123,
        asScientificNotation: 123,
        asString: "123"
      })
    )
  })

  it("converts exponent parts to a standard input", () => {
    expect(Decimal.fromExponentParts({base: 215, exponent: -2})).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 215,
          exponent: -2
        }),
        asInteger: 2,
        asNumber: 2.15,
        asScientificNotation: 215e-2,
        asString: "2.15"
      })
    )
  })
})

describe("toRadix()", () => {
  it("works with integers", () => {
    expect(new Decimal(11).toRadix(3)).toBe("102")
    expect(new Decimal(200).toRadix(11)).toBe("172")
  })

  it("works with floats", () => {
    expect(new Decimal(0.5).toRadix(4)).toBe("0.2")
  })

  it("works with scientific notation", () => {
    expect(new Decimal(3e30).toRadix(30)).toBe("8i3fls3pqmpq000000000")
  })

  it("works for static properties", () => {
    const sixteen = new Decimal(16)

    expect(sixteen.asBinary).toBe("10000")
    expect(sixteen.asOctal).toBe("20")
    expect(sixteen.asHexidecimal).toBe("10")
  })

  it("recognizes range boundaries", () => {
    const zero = new Decimal(0)

    expect(zero.toRadix(1)).toEqual(
      Error("value is outside of the valid range")
    )
    expect(zero.toRadix(37)).toEqual(
      Error("value is outside of the valid range")
    )
  })
})

// --------------------------------- Comparison --------------------------------
describe("equals()", () => {
  it("sees equality of matching decimals", () => {
    const first = new Decimal(12)
    const second = first.duplicate()

    expect(first == second).toBe(false)
    expect(first === second).toBe(false)

    expect(first.equals(second)).toBe(true)
    expect(second.equals(first)).toBe(true)
  })

  it("sees inequality of mismatched decimals", () => {
    const first = new Decimal(3.4)
    const second = new Decimal(5.6)

    expect(first == second).toBe(false)
    expect(first === second).toBe(false)

    expect(first.equals(second)).toBe(false)
    expect(second.equals(first)).toBe(false)
  })

  it("sees inequality of decimals that match only in base", () => {
    const first = new Decimal(1)
    const second = new Decimal(0.1)

    expect(first == second).toBe(false)
    expect(first === second).toBe(false)

    expect(first.equals(second)).toBe(false)
    expect(second.equals(first)).toBe(false)
  })

  it("passes through RangeError from toFixed when given negative 'toPrecision' option", () => {
    expect(new Decimal(0).equals(new Decimal(0), {toPrecision: -1})).toEqual(
      Error("toFixed() digits argument must be between 0 and 100")
    )
  })

  it("compares properly with the 'toPrecision' option", () => {
    const first = new Decimal(1.001)
    const second = new Decimal(1)

    expect(first.equals(second)).toBe(false)
    expect(first.equals(second, {toPrecision: 2})).toBe(true)
    expect(first.equals(second, {toPrecision: 3})).toBe(false)
  })

  it("compares properly with the 'withinDelta' option", () => {
    const first = new Decimal(1)
    const second = new Decimal(2)

    expect(first.equals(second)).toBe(false)
    expect(first.equals(second, {withinDelta: 1})).toBe(true)
    expect(first.equals(second, {withinDelta: 0.5})).toBe(false)
    expect(first.equals(second, {withinDelta: new Decimal(0.5)})).toBe(false)
  })

  it("compares properly with the 'withinDelta' option (non-inclusive)", () => {
    const first = new Decimal(1)
    const second = new Decimal(2)

    expect(first.equals(second)).toBe(false)
    expect(first.equals(second, {inclusive: false, withinDelta: 1})).toBe(false)
    expect(first.equals(second, {inclusive: false, withinDelta: 2})).toBe(true)
    expect(first.equals(second, {inclusive: false, withinDelta: 0.5})).toBe(
      false
    )
    expect(
      first.equals(second, {inclusive: false, withinDelta: new Decimal(0.5)})
    ).toBe(false)
  })

  it("compares properly with both the 'toPrecision' and 'withinDelta' options", () => {
    const first = new Decimal(1.001)
    const second = new Decimal(1)

    expect(first.equals(second)).toBe(false)
    expect(first.equals(second, {toPrecision: 10, withinDelta: 0.5})).toBe(true)
    expect(
      first.equals(second, {
        toPrecision: 10,
        withinDelta: new Decimal(0.001),
        inclusive: false
      })
    ).toBe(false)
  })
})

describe("compare()", () => {
  it("recognizes an 'equal' comparison", () => {
    const first = new Decimal(12)
    const second = first.duplicate()

    expect(first.compare(second)).toBe(Comparator.EQ)
  })

  it("recognizes a 'less than' comparison", () => {
    expect(new Decimal(1).compare(new Decimal(2))).toBe(Comparator.LT)
    expect(new Decimal(8).compare(new Decimal(10))).toBe(Comparator.LT)
  })

  it("recognizes a 'greater than' comparison", () => {
    expect(new Decimal(4).compare(new Decimal(3))).toBe(Comparator.GT)
    expect(new Decimal(12).compare(new Decimal(9))).toBe(Comparator.GT)
  })
})

describe("withinDelta()", () => {
  it("works with integers", () => {
    const decimal = new Decimal(1)

    expect(decimal.withinDelta(1, 1)).toBe(true)
    expect(decimal.withinDelta(2, 1)).toBe(true)
    expect(decimal.withinDelta(0, 1)).toBe(true)
    expect(decimal.withinDelta(-1, 1)).toBe(false)
  })

  it("works with floats", () => {
    const decimal = new Decimal(0.1)

    // expect(decimal.withinDelta(0.1, 0.1)).toBe(true)
    // expect(decimal.withinDelta(0.2, 0.1)).toBe(true)
    // expect(decimal.withinDelta(0, 0.1)).toBe(true)
    expect(decimal.withinDelta(-0.1, 0.1)).toBe(false)
  })

  it("works with integers (non-inclusive)", () => {
    const decimal = new Decimal(1)

    expect(decimal.withinDelta(1, 1, false)).toBe(true)

    expect(decimal.withinDelta(2, 1, false)).toBe(false)
    expect(decimal.withinDelta(2, 2, false)).toBe(true)

    expect(decimal.withinDelta(0, 1, false)).toBe(false)
    expect(decimal.withinDelta(0, 2, false)).toBe(true)

    expect(decimal.withinDelta(-1, 1, false)).toBe(false)
  })

  it("works with floats (non-inclusive)", () => {
    const decimal = new Decimal(0.1)

    expect(decimal.withinDelta(0.1, 0.1, false)).toBe(true)

    expect(decimal.withinDelta(0.2, 0.1, false)).toBe(false)
    expect(decimal.withinDelta(0.2, 0.2, false)).toBe(true)

    expect(decimal.withinDelta(0, 0.1, false)).toBe(false)
    expect(decimal.withinDelta(0, 0.2, false)).toBe(true)

    expect(decimal.withinDelta(-0.1, 0.1, false)).toBe(false)
  })
})

// ------------------------------ Bulk conversion ------------------------------
describe("operandsToDecimals()", () => {
  it("makes decimals from each input", () => {
    Decimal.operandsToDecimals(
      new Decimal(1),
      1,
      "1"
    ).forEach((decimal: Decimal) => {
      expect(decimal instanceof Decimal).toBe(true)
      expect(decimal).toEqual(
        expect.objectContaining({
          asExponent: expect.objectContaining({
            base: 1,
            exponent: 0
          }),
          asInteger: 1,
          asNumber: 1,
          asScientificNotation: 1,
          asString: "1"
        })
      )
    })
  })
})

// ------------------------------ Sign conversion ------------------------------
describe("absolute()", () => {
  it("makes a duplicate of an already positive decimal", () => {
    const original = new Decimal(1)
    const absolute = original.absolute()

    expect(original == absolute).toBe(false)
    expect(original === absolute).toBe(false)
    expect(absolute).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("makes a negative decimal into a positive one", () => {
    expect(new Decimal(-1).absolute()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("properly uses the 'abs' alias", () => {
    expect(new Decimal(-1).abs()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })
})

describe("negate()", () => {
  it("makes a negative of a positive decimal", () => {
    expect(new Decimal(1).negate()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: -1,
          exponent: 0
        }),
        asInteger: -1,
        asNumber: -1,
        asScientificNotation: -1,
        asString: "-1"
      })
    )
  })

  it("makes a negative decimal into a positive one", () => {
    expect(new Decimal(-1).negate()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("properly uses the 'neg' alias", () => {
    expect(new Decimal(-1).neg()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })
})

// --------------------------------- Arithmetic --------------------------------
describe("add()", () => {
  it("can add decimals with the same exponent", () => {
    expect(new Decimal(0.1).add(0.2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 3,
          exponent: -1
        }),
        asInteger: 0,
        asNumber: 0.3,
        asScientificNotation: 3e-1,
        asString: "0.3"
      })
    )
    expect(new Decimal(3).add(4)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 7,
          exponent: 0
        }),
        asInteger: 7,
        asNumber: 7,
        asScientificNotation: 7,
        asString: "7"
      })
    )
    expect(new Decimal(50).add(60)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 110,
          exponent: 0
        }),
        asInteger: 110,
        asNumber: 110,
        asScientificNotation: 110,
        asString: "110"
      })
    )
  })

  it("can add multiple decimals", () => {
    expect(new Decimal(1).add(2, 3)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 6,
          exponent: 0
        }),
        asInteger: 6,
        asNumber: 6,
        asScientificNotation: 6,
        asString: "6"
      })
    )
  })

  it("can add multiple decimals of different input types", () => {
    expect(new Decimal(1).add(1, "1", new Decimal(1))).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 4,
          exponent: 0
        }),
        asInteger: 4,
        asNumber: 4,
        asScientificNotation: 4,
        asString: "4"
      })
    )
  })

  it("can add decimals with different exponents", () => {
    expect(new Decimal(10).add(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 12,
          exponent: 0
        }),
        asInteger: 12,
        asNumber: 12,
        asScientificNotation: 12,
        asString: "12"
      })
    )
    expect(new Decimal(2).add(10)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 12,
          exponent: 0
        }),
        asInteger: 12,
        asNumber: 12,
        asScientificNotation: 12,
        asString: "12"
      })
    )
    expect(new Decimal(0.1).add(0.05)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 15,
          exponent: -2
        }),
        asInteger: 0,
        asNumber: 0.15,
        asScientificNotation: 15e-2,
        asString: "0.15"
      })
    )
    expect(new Decimal(2).add(0.1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 21,
          exponent: -1
        }),
        asInteger: 2,
        asNumber: 2.1,
        asScientificNotation: 21e-1,
        asString: "2.1"
      })
    )
    expect(new Decimal(2).add(0.1, 0.05)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 215,
          exponent: -2
        }),
        asInteger: 2,
        asNumber: 2.15,
        asScientificNotation: 215e-2,
        asString: "2.15"
      })
    )
  })
})

describe("subtract()", () => {
  it("can subtract decimals with the same exponent", () => {
    expect(new Decimal(0.2).subtract(0.1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: -1
        }),
        asInteger: 0,
        asNumber: 0.1,
        asScientificNotation: 1e-1,
        asString: "0.1"
      })
    )
    expect(new Decimal(3).subtract(4)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: -1,
          exponent: 0
        }),
        asInteger: -1,
        asNumber: -1,
        asScientificNotation: -1,
        asString: "-1"
      })
    )
    expect(new Decimal(60).subtract(50)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 1
        }),
        asInteger: 10,
        asNumber: 10,
        asScientificNotation: 1e1,
        asString: "10"
      })
    )
  })

  it("can subtract multiple decimals (left to right)", () => {
    expect(new Decimal(5).subtract(2, 1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 0
        }),
        asInteger: 2,
        asNumber: 2,
        asScientificNotation: 2,
        asString: "2"
      })
    )
  })

  it("can subtract multiple decimals of different input types (left to right)", () => {
    expect(new Decimal(6).subtract(1, "1", new Decimal(1))).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 3,
          exponent: 0
        }),
        asInteger: 3,
        asNumber: 3,
        asScientificNotation: 3,
        asString: "3"
      })
    )
  })

  it("can subtract decimals with different exponents", () => {
    expect(new Decimal(10).subtract(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 8,
          exponent: 0
        }),
        asInteger: 8,
        asNumber: 8,
        asScientificNotation: 8,
        asString: "8"
      })
    )
    expect(new Decimal(2).subtract(10)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: -8,
          exponent: 0
        }),
        asInteger: -8,
        asNumber: -8,
        asScientificNotation: -8,
        asString: "-8"
      })
    )
  })

  it("properly uses the 'sub' alias", () => {
    expect(new Decimal(2).sub(1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })
})

describe("multiply()", () => {
  it("can multiply decimals with the same exponent", () => {
    expect(new Decimal(0.2).multiply(0.1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: -2
        }),
        asInteger: 0,
        asNumber: 0.02,
        asScientificNotation: 2e-2,
        asString: "0.02"
      })
    )
    expect(new Decimal(3).multiply(4)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 12,
          exponent: 0
        }),
        asInteger: 12,
        asNumber: 12,
        asScientificNotation: 12,
        asString: "12"
      })
    )
  })

  it("can multiply decimals with different exponents", () => {
    expect(new Decimal(10).multiply(3)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 3,
          exponent: 1
        }),
        asInteger: 30,
        asNumber: 30,
        asScientificNotation: 3e1,
        asString: "30"
      })
    )
    expect(new Decimal(2).multiply(10)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 1
        }),
        asInteger: 20,
        asNumber: 20,
        asScientificNotation: 2e1,
        asString: "20"
      })
    )
  })

  it("can multiply multiple decimals", () => {
    expect(new Decimal(3).multiply(2, 1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 6,
          exponent: 0
        }),
        asInteger: 6,
        asNumber: 6,
        asScientificNotation: 6,
        asString: "6"
      })
    )
  })

  it("can multiply multiple decimals of different input types", () => {
    expect(new Decimal(2).multiply(2, "2", new Decimal(2))).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 16,
          exponent: 0
        }),
        asInteger: 16,
        asNumber: 16,
        asScientificNotation: 16,
        asString: "16"
      })
    )
  })

  it("properly uses the 'mult' alias", () => {
    expect(new Decimal(2).mult(4)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 8,
          exponent: 0
        }),
        asInteger: 8,
        asNumber: 8,
        asScientificNotation: 8,
        asString: "8"
      })
    )
  })
})

describe("divide()", () => {
  it("can divide decimals with the same exponent", () => {
    expect(new Decimal(4).divide(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 0
        }),
        asInteger: 2,
        asNumber: 2,
        asScientificNotation: 2,
        asString: "2"
      })
    )
    expect(new Decimal(5).divide(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 25,
          exponent: -1
        }),
        asInteger: 2,
        asNumber: 2.5,
        asScientificNotation: 25e-1,
        asString: "2.5"
      })
    )
  })

  it("can divide decimals with different exponents", () => {
    expect(new Decimal(30).divide(3)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 1
        }),
        asInteger: 10,
        asNumber: 10,
        asScientificNotation: 1e1,
        asString: "10"
      })
    )
  })

  it("can divide multiple decimals (left to right)", () => {
    expect(new Decimal(20).divide(5, 4)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("can divide multiple decimals of different input types (left to right)", () => {
    expect(new Decimal(16).divide(2, "2", new Decimal(2))).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 0
        }),
        asInteger: 2,
        asNumber: 2,
        asScientificNotation: 2,
        asString: "2"
      })
    )
  })

  it("properly uses the 'div' alias", () => {
    expect(new Decimal(4).div(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 0
        }),
        asInteger: 2,
        asNumber: 2,
        asScientificNotation: 2,
        asString: "2"
      })
    )
  })

  it("doesn't divide by zero", () => {
    expect(new Decimal(1).divide(0)).toEqual(Error("cannot divide by zero"))
    expect(new Decimal(1).divide("0")).toEqual(Error("cannot divide by zero"))
    expect(new Decimal(1).divide(new Decimal(0))).toEqual(
      Error("cannot divide by zero")
    )
  })

  it.skip("repeating figure test", () => {
    expect(new Decimal(1).divide(3)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 3333333333333333,
          exponent: -16
        }),
        asInteger: 0,
        asNumber: 0.3333333333333333,
        asScientificNotation: 3333333333333333e-16,
        asString: "0.3333333333333333"
      })
    )
    expect(new Decimal(1).divide(7)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 14285714285714284,
          exponent: -17
        }),
        asInteger: 0,
        asNumber: 0.14285714285714285,
        asScientificNotation: 14285714285714285e-17,
        asString: "0.14285714285714284"
      })
    )
  })
})

describe("modulo()", () => {
  it("can modulo decimals with the same exponent", () => {
    expect(new Decimal(4).modulo(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 0,
          exponent: 0
        }),
        asInteger: 0,
        asNumber: 0,
        asScientificNotation: 0,
        asString: "0"
      })
    )
    expect(new Decimal(5).modulo(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("can modulo decimals with different exponents", () => {
    expect(new Decimal(30).modulo(5)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 0,
          exponent: 0
        }),
        asInteger: 0,
        asNumber: 0,
        asScientificNotation: 0,
        asString: "0"
      })
    )
    expect(new Decimal(30).modulo(7)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 0
        }),
        asInteger: 2,
        asNumber: 2,
        asScientificNotation: 2,
        asString: "2"
      })
    )
  })

  it("can modulo decimals that result in an exponent", () => {
    expect(new Decimal(40).modulo(30)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 1
        }),
        asInteger: 10,
        asNumber: 10,
        asScientificNotation: 1e1,
        asString: "10"
      })
    )
  })

  it("can modulo multiple decimals (left to right)", () => {
    expect(new Decimal(7).modulo(4, 2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("can modulo multiple decimals of different input types (left to right)", () => {
    expect(new Decimal(9).modulo(7, "5", new Decimal(4))).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 0
        }),
        asInteger: 2,
        asNumber: 2,
        asScientificNotation: 2,
        asString: "2"
      })
    )
  })

  it("properly uses the the aliases", () => {
    const matcher = expect.objectContaining({
      asExponent: expect.objectContaining({
        base: 3,
        exponent: 0
      }),
      asInteger: 3,
      asNumber: 3,
      asScientificNotation: 3,
      asString: "3"
    })

    expect(new Decimal(7).mod(4)).toEqual(matcher)
    expect(new Decimal(7).remainder(4)).toEqual(matcher)
    expect(new Decimal(7).rem(4)).toEqual(matcher)
  })

  it("doesn't modulo by zero", () => {
    expect(new Decimal(1).modulo(0)).toEqual(Error("cannot divide by zero"))
    expect(new Decimal(1).modulo("0")).toEqual(Error("cannot divide by zero"))
    expect(new Decimal(1).modulo(new Decimal(0))).toEqual(
      Error("cannot divide by zero")
    )
  })
})

describe("exponentiate()", () => {
  it("can exponentiate decimals with the same exponent", () => {
    expect(new Decimal(4).exponentiate(2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 16,
          exponent: 0
        }),
        asInteger: 16,
        asNumber: 16,
        asScientificNotation: 16,
        asString: "16"
      })
    )
    expect(new Decimal(4).exponentiate(-1)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 25,
          exponent: -2
        }),
        asInteger: 0,
        asNumber: 0.25,
        asScientificNotation: 25e-2,
        asString: "0.25"
      })
    )
  })

  it("can exponentiate decimals with different exponents", () => {
    expect(new Decimal(1).exponentiate(10)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
    expect(new Decimal(20).exponentiate(3)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 8,
          exponent: 3
        }),
        asInteger: 8000,
        asNumber: 8000,
        asScientificNotation: 8e3,
        asString: "8,000"
      })
    )
  })

  it("can exponentiate multiple decimals (left to right)", () => {
    expect(new Decimal(2).exponentiate(3, 2)).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 64,
          exponent: 0
        }),
        asInteger: 64,
        asNumber: 64,
        asScientificNotation: 64,
        asString: "64"
      })
    )
  })

  it("can exponentiate multiple decimals of different input types (left to right)", () => {
    expect(new Decimal(2).exponentiate(2, "2", new Decimal(2))).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 256,
          exponent: 0
        }),
        asInteger: 256,
        asNumber: 256,
        asScientificNotation: 256,
        asString: "256"
      })
    )
    expect(new Decimal(1).exponentiate(2, "3", new Decimal(4))).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
  })

  it("properly uses the the aliases", () => {
    const matcher = expect.objectContaining({
      asExponent: expect.objectContaining({
        base: 16,
        exponent: 0
      }),
      asInteger: 16,
      asNumber: 16,
      asScientificNotation: 16,
      asString: "16"
    })

    expect(new Decimal(4).exp(2)).toEqual(matcher)
    expect(new Decimal(4).power(2)).toEqual(matcher)
    expect(new Decimal(4).pow(2)).toEqual(matcher)
    expect(new Decimal(4).raise(2)).toEqual(matcher)
  })
})

describe("squareRoot()", () => {
  it("finds a square root when possible", () => {
    expect(new Decimal(1).squareRoot()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 1,
          exponent: 0
        }),
        asInteger: 1,
        asNumber: 1,
        asScientificNotation: 1,
        asString: "1"
      })
    )
    expect(new Decimal(4).squareRoot()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 2,
          exponent: 0
        }),
        asInteger: 2,
        asNumber: 2,
        asScientificNotation: 2,
        asString: "2"
      })
    )
    expect(new Decimal(0.25).squareRoot()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 5,
          exponent: -1
        }),
        asInteger: 0,
        asNumber: 0.5,
        asScientificNotation: 5e-1,
        asString: "0.5"
      })
    )
  })

  it("returns an error when negative", () => {
    expect(new Decimal(-1).squareRoot()).toEqual(
      Error("cannot find square root of a negative number")
    )
  })

  it("properly uses the the 'sqrt' alias", () => {
    expect(new Decimal(16).sqrt()).toEqual(
      expect.objectContaining({
        asExponent: expect.objectContaining({
          base: 4,
          exponent: 0
        }),
        asInteger: 4,
        asNumber: 4,
        asScientificNotation: 4,
        asString: "4"
      })
    )
  })
})

// General tests
describe("common math issues in JS", () => {
  it("addition", () => {
    addTest(0.1, 0.2, 0.3)
    addTest(0.01, 0.05, 0.06)
    addTest(0.1, 0.05, 0.15)
  })

  it("subtraction", () => {
    subTest(0.3, 0.1, 0.2)
  })

  it("multiplication", () => {
    multTest(0.1, 0.2, 0.02)
    multTest(0.1, 3, 0.3)
    multTest(0.3, 3, 0.9)
  })
})
