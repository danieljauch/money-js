import {DecimalOperand} from "./decimal.d"

export type InputShape = string | number

export type Delimiter = "." | "," | " "

export enum Comparator {
  LT = -1,
  EQ = 0,
  GT = 1
}

export interface EqualsOptions {
  toPrecision?: number
  withinDelta?: DecimalOperand
  inclusive?: boolean
}
