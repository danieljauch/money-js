import {Range} from "./range.d"
import {Comparator} from "../types"

import {inRange} from "./range"

describe("inRange", () => {
  it("works with number ranges", () => {
    const range: Range<number> = [0, 10]

    expect(inRange(-1, range)).toBe(false)
    expect(inRange(0, range)).toBe(true)
    expect(inRange(5, range)).toBe(true)
    expect(inRange(10, range)).toBe(true)
    expect(inRange(11, range)).toBe(false)
  })

  it("works with string ranges", () => {
    const range: Range<string> = ["b", "f"]

    expect(inRange("a", range)).toBe(false)
    expect(inRange("b", range)).toBe(true)
    expect(inRange("d", range)).toBe(true)
    expect(inRange("f", range)).toBe(true)
    expect(inRange("g", range)).toBe(false)
  })

  it("works with boolean ranges", () => {
    const range: Range<boolean> = [true, true]

    expect(inRange(false, range)).toBe(false)
    expect(inRange(true, range)).toBe(true)
  })

  it("works with enums", () => {
    const range: Range<Comparator> = [Comparator.EQ, Comparator.GT]

    expect(inRange(Comparator.LT, range)).toBe(false)
    expect(inRange(Comparator.EQ, range)).toBe(true)
    expect(inRange(Comparator.GT, range)).toBe(true)
  })
})
