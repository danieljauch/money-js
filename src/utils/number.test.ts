import {numberParts, numberPartsToNumber, numberPartsToString} from "./number"

describe("numberParts", () => {
  it("properly works with an integer", () => {
    expect(numberParts(1)).toEqual({left: "1", right: undefined})
    expect(numberParts(20)).toEqual({left: "20", right: undefined})
    expect(numberParts(300)).toEqual({left: "300", right: undefined})
    expect(numberParts(4000)).toEqual({left: "4000", right: undefined})
  })

  it("properly works with a float", () => {
    expect(numberParts(1)).toEqual({left: "1", right: undefined})
    expect(numberParts(0.2)).toEqual({left: "0", right: "2"})
    expect(numberParts(0.03)).toEqual({left: "0", right: "03"})
    expect(numberParts(1.004)).toEqual({left: "1", right: "004"})
  })

  it("properly accepts a string input", () => {
    expect(numberParts("1")).toEqual({left: "1", right: undefined})
    expect(numberParts("0.2")).toEqual({left: "0", right: "2"})
    expect(numberParts("0.03")).toEqual({left: "0", right: "03"})
    expect(numberParts("1.004")).toEqual({left: "1", right: "004"})
  })

  it("allows for a custom decimal", () => {
    expect(numberParts("1", ",")).toEqual({left: "1", right: undefined})
    expect(numberParts("0,2", ",")).toEqual({left: "0", right: "2"})
    expect(numberParts("0,03", ",")).toEqual({left: "0", right: "03"})
    expect(numberParts("4.005,67", ",")).toEqual({left: "4005", right: "67"})
  })
})

describe("numberPartsToNumber", () => {
  it("properly works with a number in any format", () => {
    expect(numberPartsToNumber(numberParts(1))).toBe(1)
    expect(numberPartsToNumber(numberParts(2.3))).toBe(2.3)
    expect(numberPartsToNumber(numberParts(4e2))).toBe(400)
  })
})

describe("numberPartsToString", () => {
  it("properly works with a number in any format", () => {
    expect(numberPartsToString(numberParts(1))).toBe("1")
    expect(numberPartsToString(numberParts(2.3))).toBe("2.3")
    expect(numberPartsToString(numberParts(4e2))).toBe("400")
  })
})
