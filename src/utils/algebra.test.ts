import {add, sub, mult, div, rem, pow, sqrt} from "./algebra"

describe("add()", () => {
  it("doesn't operate on NaN's", () => {
    expect(add(1, NaN)).toEqual(Error("cannot operate on NaN"))
  })

  it("warns about operating on Infinity", () => {
    expect(add(1, Infinity)).toEqual(
      Error("operation resulted in infinite value")
    )
  })
})

describe("sub()", () => {
  it("doesn't operate on NaN's", () => {
    expect(sub(1, NaN)).toEqual(Error("cannot operate on NaN"))
  })

  it("warns about operating on Infinity", () => {
    expect(sub(1, Infinity)).toEqual(
      Error("operation resulted in infinite value")
    )
  })
})

describe("mult()", () => {
  it("doesn't operate on NaN's", () => {
    expect(mult(1, NaN)).toEqual(Error("cannot operate on NaN"))
  })

  it("warns about operating on Infinity", () => {
    expect(mult(1, Infinity)).toEqual(
      Error("operation resulted in infinite value")
    )
  })
})

describe("div()", () => {
  it("doesn't operate on NaN's", () => {
    expect(div(1, NaN)).toEqual(Error("cannot operate on NaN"))
  })

  it("warns about operating on Infinity", () => {
    expect(div(Infinity, 1)).toEqual(
      Error("operation resulted in infinite value")
    )
  })

  it("doesn't allow dividing by zero", () => {
    expect(div(2, 1)).toBe(2)
    expect(div(2, 0)).toEqual(Error("cannot divide by zero"))
  })
})

describe("rem()", () => {
  it("doesn't operate on NaN's", () => {
    expect(rem(1, NaN)).toEqual(Error("cannot operate on NaN"))
  })

  it.skip("warns about operating on Infinity", () => {
    // I can't find a remainder that results in Infinity
  })

  it("doesn't allow dividing by zero", () => {
    expect(rem(2, 1)).toBe(0)
    expect(rem(2, 0)).toEqual(Error("cannot divide by zero"))
  })
})

describe("pow()", () => {
  it("doesn't operate on NaN's", () => {
    expect(pow(1, NaN)).toEqual(Error("cannot operate on NaN"))
  })

  it("warns about operating on Infinity", () => {
    expect(pow(Infinity, 1)).toEqual(
      Error("operation resulted in infinite value")
    )
  })
})

describe("sqrt()", () => {
  it("doesn't operate on NaN's", () => {
    expect(sqrt(NaN)).toEqual(Error("cannot operate on NaN"))
  })

  it("warns about operating on Infinity", () => {
    expect(sqrt(Infinity)).toEqual(
      Error("operation resulted in infinite value")
    )
  })

  it("warns about an operation resulting in Infinity", () => {
    expect(sqrt(Infinity)).toEqual(
      Error("operation resulted in infinite value")
    )
  })

  it("doesn't allow the square of a negative number", () => {
    expect(sqrt(4)).toBe(2)
    expect(sqrt(-1)).toEqual(
      Error("cannot find square root of a negative number")
    )
  })

  it("works for squares", () => {
    expect(sqrt(1)).toBe(1)
    expect(sqrt(4)).toBe(2)
    expect(sqrt(9)).toBe(3)
    expect(sqrt(16)).toBe(4)
  })
})
