import {Delimiter} from "../types"

export interface ExponentParts {
  [index: string]: number
  base: number
  exponent: number
}

export interface ExponentOptions {
  delimiter?: Delimiter
  decimal?: Delimiter
  withExponent?: number
}
