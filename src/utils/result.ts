import {Result} from "./result.d"

export const isError = <SuccessT>(result: Result<SuccessT>): result is Error =>
  result instanceof Error

export const isSuccess = <SuccessT>(
  result: Result<SuccessT>
): result is SuccessT => !isError(result)
