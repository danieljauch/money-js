import {NumberOptions, NumberParts} from "./number.d"
import {Delimiter, InputShape} from "../types"

import {delimiterRejoin, normalizeNumber, rejoinParts} from "./normalize"

export function numberParts (
  rawInput: InputShape,
  decimal: Delimiter = "."
): NumberParts {
  const normalizedNumber = normalizeNumber(rawInput, decimal)
  const [left, right] = String(normalizedNumber).split(".")
  return {left, right}
}

export function numberPartsToString (
  parts: NumberParts,
  options?: NumberOptions
): string {
  if (options) return delimiterRejoin(parts, options.delimiter, options.decimal)
  const {left, right} = parts
  return rejoinParts([left, right], ".")
}

export function numberPartsToNumber (parts: NumberParts): number {
  return Number(numberPartsToString(parts))
}
