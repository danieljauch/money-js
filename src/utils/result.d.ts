export type Result<SuccessT> = SuccessT | Error
