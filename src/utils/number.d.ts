import {Delimiter} from "../types"

export interface NumberParts {
  [index: string]: string | undefined
  left: string
  right?: string
}

export interface NumberOptions {
  decimal: Delimiter
  delimiter: Delimiter
}
