import {ExponentOptions, ExponentParts} from "./exponent.d"
import {InputShape} from "../types"

import {normalizeNumber, rejoinParts} from "./normalize"
import {numberParts} from "./number"

const defaultOptions: ExponentOptions = {
  delimiter: ",",
  decimal: "."
}

export default class Exponent {
  base: number
  exponent: number
  asString: string
  asNumber: number

  constructor (
    rawInput: InputShape,
    options: ExponentOptions = defaultOptions
  ) {
    const {base, exponent} = this.getParts(rawInput, options)

    this.base = base
    this.exponent = exponent
    this.asString = this.toString()
    this.asNumber = Number(this.asString)
  }

  private getParts = (
    rawInput: InputShape,
    options: ExponentOptions = defaultOptions
  ): ExponentParts => {
    const {decimal = defaultOptions.decimal, withExponent} = options
    const normalizedNumber = normalizeNumber(rawInput, decimal)
    const [stringBase, stringExponent] = normalizeNumber(rawInput)
      .toExponential()
      .split("e")
    const base = parseFloat(stringBase)
    const exponent = parseInt(stringExponent)
    let output

    if (Number.isInteger(base)) output = {base, exponent}
    else {
      const {left, right} = numberParts(normalizedNumber)

      if (!right) output = {base: parseInt(left), exponent: 0}
      else output = {base: Number(left + right), exponent: -right.length}
    }

    if (withExponent !== undefined && withExponent !== output.exponent)
      return {
        exponent: withExponent,
        base:
          withExponent === 0
            ? Number(normalizedNumber)
            : base * Math.pow(10, exponent + Math.abs(withExponent))
      }

    return output
  }

  private toString = (): string => {
    if (this.base === 0) return "0"
    if (this.exponent === 0) return String(this.base)
    return rejoinParts([this.base, this.exponent], "e")
  }
}
