import {NumberParts} from "./number.d"
import {Delimiter, InputShape} from "../types"

export const delimiterRejoin = (
  {left, right}: NumberParts,
  delimiter: Delimiter = ",",
  decimal: Delimiter = "."
): string => {
  const stringLeft = String(left)
  if (stringLeft.length <= 3) {
    if (!right) return stringLeft
    return stringLeft + decimal + String(right)
  }

  const leftDigits = stringLeft.split("")
  const leftPadding = leftDigits.length % 3
  const leftThread = []

  for (let i = leftDigits.length - 1; i >= 0; i--) {
    leftThread.push(leftDigits[i])
    if (i !== 0 && i > leftPadding - 1 && (i - leftPadding) % 3 === 0)
      leftThread.push(delimiter)
  }

  const delimitedLeft = leftThread.reverse().join("")
  if (!right) return delimitedLeft
  return delimitedLeft + decimal + String(right)
}

export const normalizeNumber = (
  rawInput: InputShape,
  decimal: Delimiter = "."
): number => {
  if (typeof rawInput === "number") return rawInput
  let output = rawInput
  if (decimal !== ".") output = rawInput.replace(".", "").replace(decimal, ".")
  return Number(output.replace(/[^\d.e-]+/, ""))
}

export const rejoinParts = (
  parts: Array<string | number | undefined>,
  joiner: string
): string => parts.filter(value => !!value).join(joiner)
