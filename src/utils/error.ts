import Lexicon from "./lexicon"

export const errorByName = (name: keyof typeof Lexicon.error): Error =>
  new Error(Lexicon.error[name])
