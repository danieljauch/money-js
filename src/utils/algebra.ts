import {Result} from "./result.d"
import {Equation} from "./algebra.d"

import {errorByName} from "./error"
import {isError} from "./result"

const safeOperation = (result: Result<number>): Result<number> => {
  if (isError(result)) return result
  if (Number.isNaN(result)) return errorByName("NaN")
  if (!Number.isFinite(result)) return errorByName("Infinite")
  return result
}

// --------------------------- Addition / Subtraction --------------------------
export const add: Equation = (l, r) => safeOperation(l + r)
export const sub: Equation = (l, r) => safeOperation(l - r)

// ------------------------- Multiplication / Division -------------------------
export const mult: Equation = (l, r) => safeOperation(l * r)
export const div: Equation = (l, r) =>
  r === 0 ? errorByName("divideByZero") : safeOperation(l / r)
export const rem: Equation = (l, r) =>
  r === 0 ? errorByName("divideByZero") : safeOperation(l % r)

// ----------------------------------- Powers ----------------------------------
export const pow: Equation = (l, r) => safeOperation(l ** r)
export const sqrt: Equation = n => {
  if (Number.isNaN(n)) errorByName("NaN")
  if (n < 0) return errorByName("negSquareRoot")
  return safeOperation(Math.sqrt(n))
}
