import Exponent from "./exponent"
import {delimiterRejoin, normalizeNumber, rejoinParts} from "./normalize"
import {numberParts} from "./number"

describe("delimiterRejoin", () => {
  it("properly formats the left side with a delimiter", () => {
    expect(delimiterRejoin({left: "1"})).toBe("1")
    expect(delimiterRejoin({left: "12"})).toBe("12")
    expect(delimiterRejoin({left: "123"})).toBe("123")
    expect(delimiterRejoin({left: "1234"})).toBe("1,234")
    expect(delimiterRejoin({left: "12345"})).toBe("12,345")
    expect(delimiterRejoin({left: "123456"})).toBe("123,456")
    expect(delimiterRejoin({left: "1234567"})).toBe("1,234,567")
  })

  it("properly joins the right", () => {
    expect(delimiterRejoin({left: "1", right: "2"})).toBe("1.2")
    expect(delimiterRejoin({left: "345", right: "67"})).toBe("345.67")
  })

  it("formats properly with a custom delimiter", () => {
    expect(delimiterRejoin({left: "1", right: "2"}, " ")).toBe("1.2")
    expect(delimiterRejoin({left: "1234", right: "56"}, " ")).toBe("1 234.56")
    expect(delimiterRejoin({left: "1234567"}, ".")).toBe("1.234.567")
  })

  it("formats properly with a custom decimal", () => {
    expect(delimiterRejoin({left: "1234567", right: "89"}, ".", ",")).toBe(
      "1.234.567,89"
    )
  })
})

describe("normalizeNumber", () => {
  it("removes whitespace", () => {
    expect(normalizeNumber("1 000")).toBe(1000)
  })

  it("removes delimiters", () => {
    expect(normalizeNumber("2,345")).toBe(2345)
  })

  it("removes special characters", () => {
    expect(normalizeNumber("$6.78")).toBe(6.78)
  })

  it("removes letters", () => {
    expect(normalizeNumber("900 USD")).toBe(900)
  })

  it("doesn't remove decimal points unless there's nothing to the right", () => {
    expect(normalizeNumber("0.1")).toBe(0.1)
    expect(normalizeNumber("2.0")).toBe(2)
    expect(normalizeNumber("3.00")).toBe(3)
  })

  it("knows how to handle numbers", () => {
    expect(normalizeNumber(1)).toBe(1)
    expect(normalizeNumber(1.0)).toBe(1)
    expect(normalizeNumber(0.1)).toBe(0.1)
  })

  it("accepts a custom delimiter", () => {
    expect(normalizeNumber("1,2", ",")).toBe(1.2)
    expect(normalizeNumber("3 4", " ")).toBe(3.4)
    expect(normalizeNumber("1,000.00")).toBe(1000)
    expect(normalizeNumber("9.876,543", ",")).toBe(9876.543)
  })
})

describe("rejoinParts", () => {
  it("can join numbers and still becomes a string", () => {
    expect(rejoinParts([1, 2], ".")).toEqual("1.2")
  })

  it("can join strings properly", () => {
    expect(rejoinParts([3, 4], "e")).toEqual("3e4")
  })

  it("can join exponent parts", () => {
    const {base, exponent} = new Exponent(1000)
    expect(rejoinParts([base, exponent], "e")).toEqual("1e3")
  })

  it("can join number parts", () => {
    let parts = numberParts(1)
    expect(rejoinParts([parts.left, parts.right], ".")).toEqual("1")
    parts = numberParts(2.3)
    expect(rejoinParts([parts.left, parts.right], ".")).toEqual("2.3")
    parts = numberParts(4e2)
    expect(rejoinParts([parts.left, parts.right], ".")).toEqual("400")
  })

  it("strips undefined values", () => {
    expect(rejoinParts([1, undefined], ".")).toEqual("1")
  })
})
