export default {
  error: {
    divideByZero: "cannot divide by zero",
    NaN: "cannot operate on NaN",
    negSquareRoot: "cannot find square root of a negative number",
    Infinite: "operation resulted in infinite value",
    outsideRange: "value is outside of the valid range"
  }
}
