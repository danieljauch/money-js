import {Range} from "./range.d"

export const inRange = <T>(member: T, [bottom, top]: Range<T>): boolean =>
  member >= bottom && member <= top
