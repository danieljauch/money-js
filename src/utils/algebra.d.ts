import {Result} from "./result.d"

export type Equation = (...args: number[]) => Result<number>
