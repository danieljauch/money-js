import Exponent from "./exponent"

describe("new Exponent()", () => {
  it("recognizes the exponent for an integer", () => {
    expect(new Exponent(1)).toEqual(
      expect.objectContaining({base: 1, exponent: 0})
    )
    expect(new Exponent(4005)).toEqual(
      expect.objectContaining({base: 4005, exponent: 0})
    )
  })

  it("recognizes the exponent for an integer with trailing zeroes", () => {
    expect(new Exponent(20)).toEqual(
      expect.objectContaining({base: 2, exponent: 1})
    )
    expect(new Exponent(300)).toEqual(
      expect.objectContaining({base: 3, exponent: 2})
    )
  })

  it("recognizes the exponent for a float", () => {
    expect(new Exponent(0.2)).toEqual(
      expect.objectContaining({base: 2, exponent: -1})
    )
    expect(new Exponent(3.4)).toEqual(
      expect.objectContaining({base: 34, exponent: -1})
    )
    expect(new Exponent(0.05)).toEqual(
      expect.objectContaining({base: 5, exponent: -2})
    )
    expect(new Exponent(0.006)).toEqual(
      expect.objectContaining({base: 6, exponent: -3})
    )
  })

  it("recognizes the exponent for a float with trailing zeroes", () => {
    expect(new Exponent(1.0)).toEqual(
      expect.objectContaining({base: 1, exponent: 0})
    )
    // prettier-ignore
    expect(new Exponent(0.200)).toEqual(
      expect.objectContaining({base: 2, exponent: -1})
    )
  })

  it("works with string versions of inputs too", () => {
    expect(new Exponent("1")).toEqual(
      expect.objectContaining({base: 1, exponent: 0})
    )
    expect(new Exponent("0.2")).toEqual(
      expect.objectContaining({base: 2, exponent: -1})
    )
    expect(new Exponent("300")).toEqual(
      expect.objectContaining({base: 3, exponent: 2})
    )
    expect(new Exponent("4005")).toEqual(
      expect.objectContaining({base: 4005, exponent: 0})
    )
    expect(new Exponent("6e3")).toEqual(
      expect.objectContaining({base: 6, exponent: 3})
    )
    expect(new Exponent("-7")).toEqual(
      expect.objectContaining({base: -7, exponent: 0})
    )
    expect(new Exponent("-0.8")).toEqual(
      expect.objectContaining({base: -8, exponent: -1})
    )
    expect(new Exponent("-9e7")).toEqual(
      expect.objectContaining({base: -9, exponent: 7})
    )
  })

  it("can take an exponent in the options for the method", () => {
    expect(new Exponent(1, {withExponent: 1})).toEqual(
      expect.objectContaining({base: 10, exponent: 1})
    )
    expect(new Exponent(1, {withExponent: -1})).toEqual(
      expect.objectContaining({base: 10, exponent: -1})
    )
    expect(new Exponent(10, {withExponent: 0})).toEqual(
      expect.objectContaining({base: 10, exponent: 0})
    )
    expect(new Exponent(0.1, {withExponent: -2})).toEqual(
      expect.objectContaining({base: 10, exponent: -2})
    )
  })

  it("defaults to standard behavior if given the default exponent as an option", () => {
    expect(new Exponent(1, {withExponent: 0})).toEqual(
      expect.objectContaining({base: 1, exponent: 0})
    )
    expect(new Exponent(4005, {withExponent: 0})).toEqual(
      expect.objectContaining({base: 4005, exponent: 0})
    )
    expect(new Exponent(300, {withExponent: 2})).toEqual(
      expect.objectContaining({base: 3, exponent: 2})
    )
    expect(new Exponent(0.2, {withExponent: -1})).toEqual(
      expect.objectContaining({base: 2, exponent: -1})
    )
  })

  it("rebases when there are numbers on both sides of the decimal point", () => {
    expect(new Exponent(1.2, {withExponent: 0})).toEqual(
      expect.objectContaining({base: 1.2, exponent: 0})
    )
  })
})

describe("asNumber", () => {
  it("properly works with an integer", () => {
    expect(new Exponent(1).asNumber).toBe(1)
    expect(new Exponent(20).asNumber).toBe(2e1)
    expect(new Exponent(300).asNumber).toBe(3e2)
    expect(new Exponent(4005).asNumber).toBe(4005)
  })

  it("properly works with a float", () => {
    expect(new Exponent(1.0).asNumber).toBe(1)
    expect(new Exponent(0.2).asNumber).toBe(2e-1)
    expect(new Exponent(3.4).asNumber).toBe(34e-1)
    expect(new Exponent(4.005).asNumber).toBe(4005e-3)
  })
})

describe("asString", () => {
  it("properly works with an integer", () => {
    expect(new Exponent(1).asString).toBe("1")
    expect(new Exponent(20).asString).toBe("2e1")
    expect(new Exponent(300).asString).toBe("3e2")
    expect(new Exponent(4005).asString).toBe("4005")
  })

  it("properly works with a float", () => {
    expect(new Exponent(1.0).asString).toBe("1")
    expect(new Exponent(0.2).asString).toBe("2e-1")
    expect(new Exponent(3.4).asString).toBe("34e-1")
    expect(new Exponent(4.005).asString).toBe("4005e-3")
  })
})
