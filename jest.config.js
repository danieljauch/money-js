module.exports = {
  collectCoverageFrom: [
    "**/*.ts",
    "**/*.tsx",
    "!**/coverage/**",
    "!**/*.json",
    "!**/*.config.js",
    "**/*.d.ts"
  ],
  moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
  reporters: ["default"],
  roots: ["<rootDir>/src"],
  testPathIgnorePatterns: [".*/node_modules", ".*/coverage"],
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(j|t)sx?$",
  transform: {
    "^.+\\.(j|t)sx?$": "ts-jest"
  }
}
