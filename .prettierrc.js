module.exports = {
  arrowParens: "avoid",
  bracketSpacing: false,
  printWidth: 80,
  semi: false,
  singleQuote: false,
  tabWidth: 2,
  trailingComma: "none"
}
